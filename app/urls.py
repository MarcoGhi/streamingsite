from django.conf.urls import url

from . import views

urlpatterns = [
    url(r'^$', views.index, name='index'),
    url(r'^search/(?P<s>[A-z0-9\w\ ]+)/', views.search, name='api'),
    url(r'^source/(?P<film_id>[A-z0-9\w\ ]+)/', views.get_source, name='source'),
    #url(r'^getfilm/(?P<s>[A-z0-9\w\ ]+)/', views.get_film, name='api'),
    url(r'^category/(?P<cat_id>[A-z0-9\w\ ]+)/$', views.category, name='category'),
    url(r'^play/(?P<film_id>[A-z0-9\w\ ]+)/$', views.play, name='play'),
]