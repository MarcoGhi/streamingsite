from django.db import models
from django.utils import timezone
import os
from django.core.files import File
from io import BytesIO

# Films(id, name, desc, publish_date, img, url, cat_id)
# Urls (id, url, film_id)
# Categories (id, cat)


class Categories(models.Model):
    name = models.CharField(max_length=25)


class Films(models.Model):

    id = models.CharField(primary_key=True, max_length=20, default='')
    title = models.CharField(max_length=50)
    original_title = models.CharField(max_length=50, default='')
    desc = models.TextField(default='')
    image_file = models.ImageField(upload_to='images', default='')
    image_url = models.URLField(default='')
    cover_image = models.URLField(default='')
    publication_year = models.IntegerField(default=0)
    created_at = models.DateField(auto_now_add=True)
    duration = models.IntegerField(default=0)
    rating = models.IntegerField(default=0)
    country = models.CharField(max_length=10, default='')
    views = models.IntegerField(default=0)
    trailer = models.URLField(default='')
    cat_id = models.ForeignKey(Categories, on_delete=models.CASCADE, default=-1)

    '''def save_film(self):
        if self.image_url and not self.image_file:
            # opener = urllib.request.build_opener()
            # opener.addheaders = [('User-Agent','Mozilla/5.0')]
            # urllib.request.install_opener(opener)
            scraper = cfscrape.create_scraper()
            result = scraper.get(self.image_url).content
            fp = BytesIO()
            fp.write(result)
            # result = urllib.request.urlretrieve(self.image_url)
            self.image_file.save(
                os.path.basename(self.image_url),
                File(fp)
            )
            self.save()'''


class Urls(models.Model):
    provider = models.CharField(max_length=20, default='')
    film_url = models.URLField()
    source_url = models.URLField(default='')
    url_type = models.CharField(max_length=20, default='')
    is_hd = models.BooleanField(default=False)
    film_id = models.ForeignKey(Films, on_delete=models.CASCADE)

