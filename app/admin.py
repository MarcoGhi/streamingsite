from django.contrib import admin

# Register your models here.
from .models import Films, Categories, Urls

admin.site.register(Films)
admin.site.register(Categories)
admin.site.register(Urls)
