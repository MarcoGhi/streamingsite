# -*- coding: utf-8 -*-
# Generated by Django 1.11.6 on 2017-11-26 12:16
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('app', '0010_auto_20171126_1141'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='films',
            name='release_date',
        ),
        migrations.AddField(
            model_name='films',
            name='publication_year',
            field=models.IntegerField(default=0),
        ),
    ]
